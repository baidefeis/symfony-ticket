<?php
/**
 * Created by PhpStorm.
 * User: ivangalan
 * Date: 24/2/18
 * Time: 10:42
 */

namespace App\AuxMethods;

use App\Entity\Category;
use App\Entity\Event;
use Doctrine\Common\Persistence\ObjectManager;

class AuxMethods
{
    private $em;

    public function __construct(ObjectManager $em) {
        $this->em = $em;
    }

    public function getCategorias() {
        return $categorias = $this->em->getRepository(Category::class)->findAll();
    }

    public function getCiudades() {
        return $ciudades = $this->em->getRepository(Event::class)->getCiudades();
    }
}