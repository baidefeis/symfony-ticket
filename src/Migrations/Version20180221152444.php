<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180221152444 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, categoria VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event (id INT AUTO_INCREMENT NOT NULL, usuario INT DEFAULT NULL, categoria INT DEFAULT NULL, imagen VARCHAR(255) NOT NULL, titulo VARCHAR(255) NOT NULL, fecha_inicio_venta DATETIME NOT NULL, fecha_fin_venta DATETIME NOT NULL, fecha_hora_evento DATETIME NOT NULL, numero_entradas INT NOT NULL, precio_entrada NUMERIC(10, 0) NOT NULL, descripcion VARCHAR(255) NOT NULL, localizacion VARCHAR(255) NOT NULL, ciudad VARCHAR(255) NOT NULL, web VARCHAR(255) DEFAULT NULL, INDEX IDX_3BAE0AA72265B05D (usuario), INDEX IDX_3BAE0AA74E10122D (categoria), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ticket (id INT AUTO_INCREMENT NOT NULL, usuario INT DEFAULT NULL, evento INT DEFAULT NULL, cantidad INT NOT NULL, INDEX IDX_97A0ADA32265B05D (usuario), INDEX IDX_97A0ADA347860B05 (evento), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, avatar VARCHAR(255) NOT NULL, role VARCHAR(255) NOT NULL, salt VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA72265B05D FOREIGN KEY (usuario) REFERENCES user (id)');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA74E10122D FOREIGN KEY (categoria) REFERENCES category (id)');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA32265B05D FOREIGN KEY (usuario) REFERENCES user (id)');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA347860B05 FOREIGN KEY (evento) REFERENCES event (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA74E10122D');
        $this->addSql('ALTER TABLE ticket DROP FOREIGN KEY FK_97A0ADA347860B05');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA72265B05D');
        $this->addSql('ALTER TABLE ticket DROP FOREIGN KEY FK_97A0ADA32265B05D');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE event');
        $this->addSql('DROP TABLE ticket');
        $this->addSql('DROP TABLE user');
    }
}
