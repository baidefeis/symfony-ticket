<?php
/**
 * Created by PhpStorm.
 * User: ivangalan
 * Date: 24/2/18
 * Time: 22:04
 */

namespace App\BLL;

use App\Entity\Message;
use App\Entity\Ticket;
use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class MessageBLL extends BaseBLL{

    public function toArray($message) {
        if ( is_null ($message))
            return null;

        if (!($message instanceof Message))
            throw new \Exception("La entidad no es un ticket");

        return [
            'id' => $message->getId(),
            'destinatario' => $message->getDestinatario()->getNombre(),
            'mensaje' => $message->getMensaje()
        ];
    }

    public function getAll() {
        $tickets = $this->em->getRepository(Ticket::class)->findBy(['usuario' => $this->getUser()]);
        return $this->entitiesToArray($tickets);
    }

    public function nuevo($data)
    {
        $message = new Message();
        $message->setRemitente($this->getUser());
        $message->setDestinatario($this->em->getRepository(User::class)->find($data['destinatario']));
        $message->setMensaje($data['mensaje']);

        return $this->guardaValidando($message);
    }
}