<?php
/**
 * Created by PhpStorm.
 * User: ivangalan
 * Date: 24/2/18
 * Time: 22:04
 */

namespace App\BLL;

use App\Entity\Event;
use App\Entity\Ticket;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class TicketBLL extends BaseBLL{

    public function toArray($ticket) {
        if ( is_null ($ticket))
            return null;

        if (!($ticket instanceof Ticket))
            throw new \Exception("La entidad no es un ticket");

        return [
            'id' => $ticket->getId(),
            'evento' => $ticket->getEvento()->getTitulo(),
            'cantidad' => $ticket->getCantidad()
        ];
    }

    public function getAll() {
        $tickets = $this->em->getRepository(Ticket::class)->findBy(['usuario' => $this->getUser()]);
        return $this->entitiesToArray($tickets);
    }

    public function getSold() {
        $tickets = $this->em->getRepository(Ticket::class)->findTicketsFromEvents($this->getUser()->getId());

        return $tickets;
    }

    public function nuevo($data)
    {
        $ticket = new Ticket();
        $ticket->setUsuario($this->getUser());
        $ticket->setEvento($this->em->getRepository(Event::class)->find($data['evento']));
        $ticket->setCantidad($data['cantidad']);

        return $this->guardaValidando($ticket);
    }
}