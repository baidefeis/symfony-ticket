<?php
/**
 * Created by PhpStorm.
 * User: ivangalan
 * Date: 24/2/18
 * Time: 17:23
 */

namespace App\BLL;


use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserBLL extends BaseBLL
{
    /**
     * @var UserPasswordEncoderInterface $encoder
     */
    private $encoder;

    /**
     * @var JWTTokenManagerInterface
     */
    private $jwtManager;

    public function setJWTManager(JWTTokenManagerInterface $jwtManager)
    {
        $this->jwtManager = $jwtManager;
    }

    public function setEncoder(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function nuevo($nombre, $password, $email)
    {
        $user = new User();

        $user->setNombre($nombre);
        $user->setPassword($this->encoder->encodePassword($user, $password));
        $user->setEmail($email);
        $user->setRole('ROLE_USER');
        $user->setAvatar('default.png');
        $user->setSalt('');

        return $this->guardaValidando($user);
    }

    public function getAll()
    {
        $users = $this->em->getRepository(User::class)->findAll();

        return $this->entitiesToArray($users);
    }

    public function toArray($user)
    {
        if ( is_null ($user))
            return null;

        if (!($user instanceof User))
            throw new \Exception("La entidad no es un User");

        return [
            'id' => $user->getId(),
            'nombre' => $user->getNombre(),
            'email' => $user->getEmail(),
            'role' => $user->getRole()
        ];
    }

    public function profile()
    {
        $user = $this->getUser();

        return $this->toArray($user);
    }

    public function cambiaPassword($nuevoPassword)
    {
        $user = $this->getUser();

        $user->setPassword($this->encoder->encodePassword($user, $nuevoPassword));

        return $this->guardaValidando($user);
    }

    public function getTokenByEmail($email)
    {
        $user = $this->em->getRepository(User:: class )
            ->findOneBy(array('email'=>$email));

        if ( is_null ($user))
            throw new AccessDeniedHttpException('Usuario no autorizado');

        return $this->jwtManager->create($user);
    }
}