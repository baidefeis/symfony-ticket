<?php
/**
 * Created by PhpStorm.
 * User: ivangalan
 * Date: 24/2/18
 * Time: 17:01
 */

namespace App\BLL;


use App\Entity\Category;
use App\Entity\Event;
use App\Entity\User;
use App\Service\FileUploader;
use \Datetime;

class EventBLL extends BaseBLL
{
    /**
     * @var FileUploader $uploader
     */
    private $uploader;

    public function setUploader(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function getEventos(string $busqueda = null, $offset=null, $limit=null)
    {
        return $this->em->getRepository(Event::class)->findEvents($busqueda, $offset, $limit);
    }

    public function getNumEventos(string $busqueda=null)
    {
        return $this->em->getRepository(Event::class)->getCountProductos($busqueda);
    }

    public function eliminaEvento($id)
    {
        $evento = $this->em->getRepository(Event::class)->find($id);

        $this->em->remove($evento);
        $this->em->flush();
    }

    public function nuevo($data)
    {
        $event = new Event();
        $event->setTitulo($data['titulo']);
        $event->setPrecioEntrada($data['precio']);
        $event->setImagen('default.png');
        $event->setFechaHoraEvento(DateTime::createFromFormat('j-m-Y', $data['fechaHoraEvento']));
        $event->setFechaInicioVenta(DateTime::createFromFormat('j-m-Y', $data['fechaInicioVenta']));
        $event->setFechaFinVenta(DateTime::createFromFormat('j-m-Y', $data['fechaFinVenta']));
        $event->setNumeroEntradas($data['numeroEntradas']);
        $event->setDescripcion($data['descripcion']);
        $event->setLocalizacion($data['localizacion']);
        $event->setCiudad($data['ciudad']);

        $categoria = $this->em->getRepository(Category::class)->find($data['categoria']);
        $event->setCategoria($categoria);

        $event->setUsuario($this->getUser());

        return $this->guardaValidando($event);
    }

    public function guardaEvento(Event $event)
    {
        $this->em->persist($event);
        $this->em->flush();
    }

    public function getEventosFiltrados($titulo, $ciudad, $categoria, $order)
    {
        $eventos = $this->em->getRepository(Event:: class )->getEventsFilter($titulo, $ciudad, $categoria, $order);

        return $this->entitiesToArray($eventos);
    }

    public function update(Event $event, array $data)//se usa
    {
        $event->setTitulo($data['titulo']);
        $event->setPrecioEntrada($data['precio']);
        $event->setImagen('default.png');
        $event->setFechaHoraEvento(DateTime::createFromFormat('j-m-Y', $data['fechaHoraEvento']));
        $event->setFechaInicioVenta(DateTime::createFromFormat('j-m-Y', $data['fechaInicioVenta']));
        $event->setFechaFinVenta(DateTime::createFromFormat('j-m-Y', $data['fechaFinVenta']));
        $event->setNumeroEntradas($data['numeroEntradas']);
        $event->setDescripcion($data['descripcion']);
        $event->setLocalizacion($data['localizacion']);
        $event->setCiudad($data['ciudad']);

        $categoria = $this->em->getRepository(Category::class)->find($data['categoria']);
        $event->setCategoria($categoria);

        if ($event->getUsuario() == $this->getUser())
            return $this->guardaValidando($event);
        throw new \Exception("No tienes permisos para modificar este evento");
    }

    public function toArray($evento)
    {
        if ( is_null ($evento))
            return null;

        if (!($evento instanceof Event))
            throw new \Exception("La entidad no es un Producto");

        return [
            'id' => $evento->getId(),
            'fechaHoraEvento' => $evento->getFechaHoraEvento()->format("d-m-Y H:i:s"),
            'titulo' => $evento->getTitulo(),
            'precioEntrada' => $evento->getPrecioEntrada(),
            'imagen' => $evento->getImagen(),
            'categoria' => $evento->getCategoria()->getCategoria()
        ];
    }

    public function cambiaImagen(Event $event, $imagenB64)
    {
        $fileName = $this->uploader->saveB64Imagen($imagenB64);

        $event->setImagen($fileName);

        return $this->guardaValidando($event);
    }
}