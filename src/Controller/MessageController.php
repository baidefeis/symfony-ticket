<?php
/**
 * Created by PhpStorm.
 * User: ivangalan
 * Date: 22/2/18
 * Time: 12:35
 */

namespace App\Controller;


use App\Entity\Message;
use App\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * @Route("/messages")
 */
class MessageController extends Controller
{
    /**
     * @Route("/me", name="showMyMessages")
     * @Template("messages.html.twig")
     * @IsGranted("ROLE_USER")
     */
    public function show() {
        $doctrine = $this->getDoctrine();
        $messages = $doctrine->getRepository(Message::class)
            ->findBy(['destinatario' => $doctrine->getRepository(User::class)->find($this->getUser()->getId())]);

        return [ 'messages' => $messages ];
    }

    /**
     * @Route("/delete/{id}", name="deleteMessage")
     * @Method("DELETE")
     * @IsGranted("ROLE_USER")
     */
    public function delete(Message $message) {
        $em = $this->getDoctrine()->getManager();

        try {
            $em->remove($message);
            $em->flush();
        } catch (exception $exception) {
            return new JsonResponse(['ok' => false]);
        }

        return new JsonResponse(['ok' => true]);
    }


    /**
     * @Route("/new", name="newMessage")
     * @Template("message-form.html.twig")
     * @IsGranted("ROLE_USER")
     */
    public function new(Request $request) {

        $usuarios = $this->getDoctrine()->getRepository(User::class)->findAll();

        $listaUsuarios = [];
        foreach ($usuarios as $usuario) {
            $listaUsuarios[$usuario->getNombre()] = $usuario->getId();
        }

        $message = new Message();


        $form = $this->createFormBuilder($message)
            ->add('destinatario', ChoiceType::class, [
                'label' => 'Destinatario:',
                'attr' => ['class' => 'form-control'],
                'choices' => $listaUsuarios
            ])
            ->add('mensaje', TextareaType::class, [
                'label' => 'Mensaje:',
                'attr' => ['class' => 'form-control']
            ])
            ->add('Save', SubmitType::class, [
                'label' => 'Enviar',
                'attr' => ['class' => 'btn btn-success btn-block']
            ])
            ->getForm();


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            //TODO hacer refactor para pillar la id de la sesión
            $message->setRemitente($em->find('App:User', $this->getUser()->getId()));

            $message->setDestinatario($em->find('App:User',$message->getDestinatario()));

            $em->persist($message);
            $em->flush();

            return $this->redirectToRoute('showMyMessages');
        }

        return ['form' => $form->createView()];

    }

    /**
     * @Route("/new/{id}", name="newMessageTo")
     * @Template("message-form.html.twig")
     * @IsGranted("ROLE_USER")
     */
    public function newTo(Request $request, User $user) {

        $usuarios = $this->getDoctrine()->getRepository(User::class)->findAll();

        $listaUsuarios = [];
        foreach ($usuarios as $usuario) {
            $listaUsuarios[$usuario->getNombre()] = $usuario->getId();
        }

        $message = new Message();


        $form = $this->createFormBuilder($message)
            ->add('destinatario', TextType::class, [
                'label' => 'Destinatario:',
                'attr' => ['class' => 'form-control', 'readonly' => 'readonly', 'value' => $user->getNombre()]
            ])
            ->add('mensaje', TextareaType::class, [
                'label' => 'Mensaje:',
                'attr' => ['class' => 'form-control']
            ])
            ->add('Save', SubmitType::class, [
                'label' => 'Enviar',
                'attr' => ['class' => 'btn btn-success btn-block']
            ])
            ->getForm();


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $message->setRemitente($em->find('App:User', $this->getUser()->getId()));

            $message->setDestinatario($user);

            $em->persist($message);
            $em->flush();

            return $this->redirectToRoute('showMyMessages');
        }

        return ['form' => $form->createView()];

    }

}