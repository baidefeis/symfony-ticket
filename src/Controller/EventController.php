<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Event;
use App\Entity\Ticket;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/events")
 */
class EventController extends Controller
{
    /**
     * @Route("/", name="showAllEvents")
     * @Template("events.html.twig")
     */
    public function show() {
        $events = $this->getDoctrine()->getRepository(Event::class)->findAll();

        return ['events' => $events];
    }

    /**
     * @Route("/mine", name="showMyEvents")
     * @Template("events-mine.html.twig")
     * @IsGranted("ROLE_GESTOR")
     */
    public function showMyEvents() {
        $user = $this->getDoctrine()->getManager()->find('App:User',$this->getUser()->getId());

        $allEvents = $this->getDoctrine()->getRepository(Event::class)->findBy(['usuario' => $user]);
        $events = $this->getDoctrine()->getRepository(Ticket::class)->findTicketsFromEvents($user);
        return ['events' => $events, 'allEvents' => $allEvents];
    }

    /**
     * @Route("/find", name="showEventsFilter")
     * @Template("events.html.twig")
     */
    public function showEventsFilter(Request $request) {
        $filter = $request->request->get('search');
        $ciudad = $request->request->get('ciudad');
        $events = $this->getDoctrine()->getRepository(Event::class)->findByFilter($filter,$ciudad);

        return ['events' => $events];
    }

    /**
     * @Route("/category/{id}", name="showEventsCategory")
     * @Template("events.html.twig")
     */
    public function showEventsCategory(Category $category) {
        $events = $this->getDoctrine()->getRepository(Event::class)->findBy(['categoria' => $category]);

        return ['events' => $events];
    }


    /**
     * @Route("/delete/{id}", name="deleteEvent")
     * @Method("DELETE")
     * @IsGranted("ROLE_GESTOR")
     */
    public function delete(Event $event) {
        $em = $this->getDoctrine()->getManager();

        try {
            $em->remove($event);
            $em->flush();
        } catch (exception $exception) {
            return new JsonResponse(['ok' => false]);
        }

        return new JsonResponse(['ok' => true]);
    }


    /**
     * @Route("/new", name="showFormNewEvent")
     * @Template("events-form.html.twig")
     * @IsGranted("ROLE_GESTOR")
     */
    public function new(Request $request) {
        $evento = new Event();

        $form = $this->createFormulario($evento);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $imagen = $evento->getImagen();
            $nombreImagen = round(microtime(true) * 1000) .'.'.$imagen->guessExtension();
            $evento->setImagen($nombreImagen);
            $imagen->move($this->getParameter('image_events_path'),$nombreImagen);

            $em = $this->getDoctrine()->getManager();

            $evento->setUsuario($em->find('App:User', $this->getUser()->getId()));

            $evento->setCategoria($em->find('App:Category',$evento->getCategoria()));

            $em->persist($evento);
            $em->flush();

            return $this->redirectToRoute('showAllEvents');
        }

        return ['form' => $form->createView()];

    }

    /**
     * @Route("/edit/{id}", name="editEvent")
     * @Template("events-form.html.twig")
     * @IsGranted("ROLE_GESTOR")
     */
    public function editEvent(Request $request, Event $evento) {
        if ($evento->getUsuario() == $this->getUser()) {


            $form = $this->createFormulario($evento);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $imagen = $evento->getImagen();
                $nombreImagen = round(microtime(true) * 1000) . '.' . $imagen->guessExtension();
                $evento->setImagen($nombreImagen);
                $imagen->move($this->getParameter('image_events_path'), $nombreImagen);

                $em = $this->getDoctrine()->getManager();


                $evento->setUsuario($em->find('App:User', $this->getUser()->getId()));

                $evento->setCategoria($em->find('App:Category', $evento->getCategoria()));

                $em->persist($evento);
                $em->flush();

                return $this->redirectToRoute('showMyEvents');
            }

            return ['form' => $form->createView()];
        } else return $this->redirectToRoute('showMyEvents');


    }


    /**
     * @Route("/{id}", name="showEventDetails")
     * @Template("event-details.html.twig")
     */
    public function showDetails(Event $event, Request $request) {

        $ticket = new Ticket();

        $form = $this->createFormBuilder($ticket)
            ->add('cantidad', IntegerType::class , [
                'label' => 'Cantidad:',
                'attr' => ['class' => 'form-control w-25', 'id' => 'cantidad', 'value' => 1]
            ])
            ->add('Save',SubmitType::class, [
                'label'=>'Comprar',
                'attr' => ['class' => 'btn btn-primary']
            ])
            ->getForm();


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->getUser()) {
                $em = $this->getDoctrine()->getManager();

                $user = $em->find('App:User', $this->getUser()->getId());


                $ticket->setEvento($event);

                $ticket->setUsuario($user);

                $em->persist($ticket);
                $em->flush();

                return $this->redirectToRoute('showAllEvents');
            } else {
                return $this->redirectToRoute('login');
            }
        }

        return ['event' => $event, 'form' => $form->createView()];
    }


    public function createFormulario($evento) {
        $categorias = $this->getDoctrine()->getRepository(Category::class)->findAll();

        $listaCategorias = [];
        foreach ($categorias as $categoria) {
            $listaCategorias[$categoria->getCategoria()] = $categoria->getId();
        }

        return $form = $this->createFormBuilder($evento)
            ->add('titulo', TextType::class , [
                'label' => 'Titulo evento:',
                'attr' => ['class' => 'form-control']
            ])

            ->add('categoria', ChoiceType::class, [
                'label' => 'Tipo de evento:',
                'attr' =>['class' => 'form-control'],
                'choices' => $listaCategorias
            ])

            ->add('ciudad', TextType::class, [
                'label' => 'Ciudad:',
                'attr' => [ 'class' => 'form-control' ]
            ])

            ->add('localizacion', TextType::class , [
                'label' => 'Localización: ',
                'attr' => [ 'class' => 'form-control' ]
            ])

            ->add('fecha_hora_evento', DateType::class , [
                'label' => 'Fecha del Evento:',
                'attr' => ['class' => 'form-control'],
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])

            ->add('fecha_inicio_venta', DateType::class , [
                'label' => 'Puesta a la venta de entradas:',
                'attr' => ['class' => 'form-control'],
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])

            ->add('fecha_fin_venta', DateType::class , [
                'label' => 'Fin de venta de entradas:',
                'attr' => ['class' => 'form-control'],
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])

            ->add('descripcion', TextareaType::class , [
                'label' => 'Descripción:',
                'attr' => [ 'class' => 'form-control' ]
            ])

            ->add('precio_entrada', NumberType::class , [
                'label' => 'Precio:',
                'attr' => [
                    'class' => 'form-control',
                    'min-value' => 1
                ]
            ])

            ->add('numero_entradas', IntegerType::class , [
                'label' => 'Numero de entradas:',
                'attr' => [
                    'class' => 'form-control',
                    'min-value' => 1
                ]
            ])

            ->add('imagen', FileType::class , [
                'label' => 'Imagen del Evento',
                'attr' => [ 'class' => 'form-control' ],
                'data_class' => null
            ])

            ->add('Save',SubmitType::class, [
                'label'=>'Crear Evento',
                'attr' => ['class' => 'btn btn-success btn-block']
            ])

            ->getForm();
    }

}