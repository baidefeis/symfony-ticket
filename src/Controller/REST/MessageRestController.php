<?php
/**
 * Created by PhpStorm.
 * User: ivangalan
 * Date: 24/2/18
 * Time: 22:03
 */

namespace App\Controller\REST;

use App\BLL\MessageBLL;
use App\Entity\Message;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class MessageRestController extends BaseApiController
{
    /**
     * @Route("/messages.{_format}", name="new-message",
     * requirements={"_format": "json"},
     * defaults={"_format": "json"})
     * @Method("POST")
     */
    public function createMessage(Request $request, MessageBLL $messageBLL)
    {
        $data = $this->getContent($request);

        $message = $messageBLL->nuevo($data);

        return $this->getResponse($message, Response::HTTP_CREATED );
    }

    /**
     * @Route("/messages/{id}.{_format}", name="borrar-message",
     * requirements={"_format": "json"},
     * defaults={"_format": "json"})
     * @Method("DELETE")
     */
    public function borrarMessage(Message $message, MessageBLL $messageBLL)
    {
        if ($message->getDestinatario() == $this->getUser()) {
            $messageBLL->delete($message);
        } else {
            return $this->getResponse('No puede borrar este mensaje', Response::HTTP_UNAUTHORIZED);
        }
        return $this->getResponse(null, Response:: HTTP_NO_CONTENT );
    }
}