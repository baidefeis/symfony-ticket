<?php
/**
 * Created by PhpStorm.
 * User: ivangalan
 * Date: 24/2/18
 * Time: 16:50
 */

namespace App\Controller\REST;

use App\BLL\EventBLL;
use App\Entity\Event;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class EventRestController extends BaseApiController
{
    /**
     * @Route("/eventos.{_format}", name="get_eventos",
     * defaults={"_format": "json"},
     * requirements={"_format": "json"}
     * )
     * @Route("/eventos/ordenados/{order}", name="get_eventos_ordenados",
     *  requirements={"order": "titulo|ciudad|categoria"}
     * )
     * @Method("GET")
     */
    public function getAll(Request $request, EventBLL $eventBLL, $order='titulo')
    {
        $titulo = $request->query->get('titulo');
        $ciudad = $request->query->get('ciudad');
        $categoria = $request->query->get('categoria');

        $eventos = $eventBLL->getEventosFiltrados($titulo, $ciudad, $categoria, $order);

        return $this->getResponse($eventos);
    }


    /**
     * @Route("/eventos.{_format}", name="eventos",
     *  defaults={"_format": "json"},
     *  requirements={"_format": "json"}
     * )
     * @Method("POST")
     */
    public function post(Request $request, EventBLL $eventBLL)
    {
        $data = $this->getContent($request);

        $event = $eventBLL->nuevo($data);

        return $this->getResponse($event, Response::HTTP_CREATED );
    }

    /**
     * @Route("/eventos/{id}.{_format}", name="update_evento",
     *  requirements={"id": "\d+", "_format": "json" },
     *  defaults={"_format": "json"})
     * @Method("PUT")
     */
    public function update(Request $request, Event $event, EventBLL $eventBLL)
    {
        $data = $this->getContent($request);
        $event = $eventBLL->update($event, $data);
        return $this->getResponse($event, Response:: HTTP_OK );
    }

    /**
     * @Route("/eventos/{id}.{_format}", name="delete_evento",
     * requirements={ "id": "\d+", "_format": "json" },
     * defaults={"_format": "json"})
     * @Method("DELETE")
     */
    public function delete(Event $event, EventBLL $eventBLL)
    {
        if ($event->getUsuario() == $this->getUser()) {
            $eventBLL->delete($event);
        } else {
            return $this->getResponse('No puede borrar este evento', Response::HTTP_UNAUTHORIZED);
        }
        return $this->getResponse(null, Response:: HTTP_NO_CONTENT );
    }


}