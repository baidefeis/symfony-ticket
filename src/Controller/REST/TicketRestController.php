<?php
/**
 * Created by PhpStorm.
 * User: ivangalan
 * Date: 24/2/18
 * Time: 22:03
 */

namespace App\Controller\REST;

use App\BLL\TicketBLL;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class TicketRestController extends BaseApiController
{
    /**
     * @Route("/tickets/me.{_format}", name="tickets",
     * requirements={"_format": "json"},
     * defaults={"_format": "json"})
     * @Method("GET")
     */
    public function getAll(TicketBLL $ticketBLL )
    {
        $tickets = $ticketBLL->getAll();

        return $this->getResponse($tickets);
    }

    /**
     * @Route("/tickets/sold.{_format}", name="tickets",
     * requirements={"_format": "json"},
     * defaults={"_format": "json"})
     * @Method("GET")
     */
    public function getSold(TicketBLL $ticketBLL )
    {
        $tickets = $ticketBLL->getSold();

        return $this->getResponse($tickets);
    }

    /**
     * @Route("/tickets.{_format}", name="new-ticket",
     * requirements={"_format": "json"},
     * defaults={"_format": "json"})
     * @Method("POST")
     */
    public function buyTicket(Request $request, TicketBLL $ticketBLL)
    {
        $data = $this->getContent($request);

        $ticket = $ticketBLL->nuevo($data);

        return $this->getResponse($ticket, Response::HTTP_CREATED );
    }
}