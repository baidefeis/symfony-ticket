<?php
/**
 * Created by PhpStorm.
 * User: ivangalan
 * Date: 24/2/18
 * Time: 17:21
 */

namespace App\Controller\REST;


use App\BLL\UserBLL;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class UserRestController extends BaseApiController
{
    /**
     * @Route("/auth/register.{_format}", name="register",
     * requirements={"_format": "json"},
     * defaults={"_format": "json"})
     * @Method("POST")
     */
    public function register(Request $request, UserBLL $userBLL)
    {
        $data = $this->getContent($request);

        $user = $userBLL->nuevo(
            $data['nombre'], $data['password'], $data['email']);

        return $this->getResponse($user, Response:: HTTP_CREATED );
    }


    /**
     * @Route("/usuarios.{_format}", name="get_usuarios",
     * defaults={"_format": "json"},
     * requirements={"_format": "json"}
     * )
     * @Method("GET")
     */
    public function getAll(UserBLL $userBLL)
    {
        $users = $userBLL->getAll();

        return $this->getResponse($users);
    }

    /**
     * @Route("/usuarios/{id}.{_format}", name="delete_usuario",
     * requirements={ "id": "\d+", "_format": "json" },
     * defaults={"_format": "json"})
     * )
     * @Method("DELETE")
     */
    public function delete(User $user, UserBLL $userBLL)
    {
        $userBLL->delete($user);

        return $this->getResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/profile.{_format}", name="profile",
     * requirements={"_format": "json"},
     * defaults={"_format": "json"})
     * @Method("GET")
     */
    public function profile(UserBLL $userBLL)
    {
        $user = $userBLL->profile();

        return $this->getResponse($user);
    }

    /**
     * @Route("/profile/password.{_format}", name="cambia_password",
     * requirements={ "_format": "json" },
     * defaults={"_format": "json"})
     * @Method("PATCH")
     */
    public function cambiaPassword(Request $request, UserBLL $userBLL)
    {
        $data = $this->getContent($request);
        if ( is_null ($data['password']) || !isset($data['password']) || empty($data['password']))
            throw new BadRequestHttpException('No se ha recibido el password');

        $user = $userBLL->cambiaPassword($data['password']);

        return $this->getResponse($user);
    }

    /**
     * @Route("/auth/login/google")
     */
    public function googleLogin(Request $request, UserBLL $userBLL)
    {
        $data = $this->getContent($request);

        if ( is_null ($data['access_token'])
            || !isset($data['access_token'])
            || empty($data['access_token']))
            throw new BadRequestHttpException('No se ha recibido el token de google');

        $googleJwt = json_decode ( file_get_contents (
            "https://www.googleapis.com/plus/v1/people/me?access_token=" .
            $data['access_token']));

        $token = $userBLL->getTokenByEmail($googleJwt->emails[0]->value);

        return $this->getResponse(['token' => $token]);
    }
}