<?php
/**
 * Created by PhpStorm.
 * User: ivangalan
 * Date: 22/2/18
 * Time: 18:18
 */

namespace App\Controller;


use App\Entity\Category;
use App\Entity\User;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Tests\Encoder\PasswordEncoder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/me", name="myProfile")
     * @Template("profile.html.twig")
     * @IsGranted("ROLE_USER")
     */
    public function show() {

        $user = $this->getDoctrine()->getManager()->find(User::class, $this->getUser()->getId());
        return ['user' => $user];
    }

    /**
     * @Route("/me/editAvatar", name="editMyAvatar")
     * @Template("profile-form.html.twig")
     * @IsGranted("ROLE_USER")
     */
    public function editAvatar(Request $request) {
        //TODO sacar id usuario de la sesión
        $user = $this->getDoctrine()->getManager()->find(User::class, $this->getUser()->getId());

        $form = $this->createFormBuilder($user)
            ->add('avatar', FileType::class, [
                'label' => 'Nuevo avatar:',
                'attr' => ['class' => 'form-control'],
                'data_class' => null
            ])
            ->add('Save', SubmitType::class, [
                'label' => 'Guardar',
                'attr' => ['class' => 'btn btn-success']
            ])
            ->getForm();


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $imagen = $user->getAvatar();
            $nombreImagen = round(microtime(true) * 1000) .'.'.$imagen->guessExtension();
            $user->setAvatar($nombreImagen);
            $imagen->move($this->getParameter('image_users_path'),$nombreImagen);

            $em = $this->getDoctrine()->getManager();

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute("myProfile");

        }

        return ['form' => $form->createView()];
    }


    /**
     * @Route("/me/editPassword", name="editMyPassword")
     * @Template("profile-form.html.twig")
     * @IsGranted("ROLE_USER")
     */
    public function editPassword(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $user = $this->getDoctrine()->getManager()->find(User::class, $this->getUser()->getId());

        $form = $this->createFormBuilder($user)
            ->add('password', PasswordType::class, [
                'label' => 'Nueva contraseña:',
                'attr' => ['class' => 'form-control']
            ])
            ->add('Save', SubmitType::class, [
                'label' => 'Guardar',
                'attr' => ['class' => 'btn btn-success']
            ])
            ->getForm();


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute("myProfile");

        }

        return ['form' => $form->createView()];
    }

    /**
     * @Route("/gestion", name="editAllUsers")
     * @Template("edit-users.html.twig")
     * @IsGranted("ROLE_ADMIN")
     */
    public function editAllUsers() {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        $tipos = ['ROLE_USER', 'ROLE_GESTOR', 'ROLE_ADMIN'];

        return ['users' => $users, 'tipos' => $tipos];
    }

    /**
     * @Route("/updateRole/{id}/{role}", name="updateRole")
     * @Template("edit-users.html.twig")
     * @IsGranted("ROLE_ADMIN")
     */
    public function updateRole(User $user, $role) {
        if ($role == 'ROLE_ADMIN' || $role == 'ROLE_USER' || $role == 'ROLE_GESTOR') {
            $user->setRole($role);
            $em = $this->getDoctrine()->getManager();
            try {
                $em->persist($user);
                $em->flush();
                return new JsonResponse(['ok' => true]);
            } catch(exception $exception) {
                return new JsonResponse(['ok' => false]);
            }
        } else {
            return new JsonResponse(['ok' => false]);
        }
    }

    /**
     * @Route("/delete/{id}", name="deleteUser")
     * @Method("DELETE")
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(User $user) {
        $em = $this->getDoctrine()->getManager();

        try {
            $em->remove($user);
            $em->flush();
        } catch (exception $exception) {
            return new JsonResponse(['ok' => false]);
        }

        return new JsonResponse(['ok' => true]);
    }


    /**
     * @Route("/new", name="createUser")
     * @Template("new-user.html.twig")
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder) {

        $tipos = ['Comprador' => 'ROLE_USER', 'Gestor' => 'ROLE_GESTOR'];

        $user = new User();

        $form = $this->createFormBuilder($user)
            ->add('nombre', TextType::class, [
                'label' => 'Nombre:',
                'attr' => ['class' => 'form-control']
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email:',
                'attr' => ['class' => 'form-control']
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Contraseña:',
                'attr' => ['class' => 'form-control']
            ])
            ->add('role', ChoiceType::class, [
                'label' => 'Tipo de evento:',
                'attr' =>['class' => 'form-control'],
                'choices' => $tipos
            ])
            ->add('Save', SubmitType::class, [
                'label' => 'Guardar',
                'attr' => ['class' => 'btn btn-success']
            ])
            ->getForm();


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $user->setAvatar('default.png');
            $user->setSalt('');
            $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute("myProfile");

        }

        return ['form' => $form->createView()];


    }
}