<?php
/**
 * Created by PhpStorm.
 * User: ivangalan
 * Date: 22/2/18
 * Time: 8:43
 */

namespace App\Controller;


use App\Entity\Ticket;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/tickets")
 */
class TicketController extends Controller
{

    /**
     * @Route("/me", name="showMyTickets")
     * @Template("tickets.html.twig")
     * @IsGranted("ROLE_USER")
     */
    public function show() {
        $doctrine = $this->getDoctrine();
        $tickets = $doctrine->getRepository(Ticket::class)
            ->findBy(['usuario' => $doctrine->getRepository(User::class)->find($this->getUser()->getId())]);

        return [ 'tickets' => $tickets ];
    }
}