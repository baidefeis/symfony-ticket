<?php
/**
 * Created by PhpStorm.
 * User: ivangalan
 * Date: 9/1/18
 * Time: 18:01
 */

namespace App\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;



class PageController extends Controller
{
    /**
     * @Route("/")
     * @Template("index.html.twig")
     */
    public function index() {
        return [];
    }

}