<?php

namespace App\Repository;

use App\Entity\Ticket;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class TicketRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ticket::class);
    }

    public function findGroupBy($idUser) {
        return $this->createQueryBuilder('t')
            ->where('t.usuario = :idUser')->setParameter('idUser', $idUser)
            ->orderBy('t.evento')
            ->getQuery()
            ->execute();
    }

    public function findTicketsFromEvents($idUser) {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.evento', 'e')
            ->addSelect('e')
            ->addSelect('SUM(t.cantidad) as cant')
            ->where('e.usuario = :idUser')->setParameter('idUser', $idUser)
            ->groupBy('t.evento')
            ->getQuery()
            ->execute();
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('t')
            ->where('t.something = :value')->setParameter('value', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
