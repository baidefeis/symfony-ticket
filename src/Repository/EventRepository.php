<?php

namespace App\Repository;

use App\Entity\Event;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class EventRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Event::class);
    }

    public function findByFilter($filter, $ciudad) {

        if ($ciudad != "0") {
            $query = $this->createQueryBuilder('e')
                ->where('e.titulo LIKE :filter')->setParameter('filter', '%'.$filter.'%')
                ->andWhere('e.ciudad = :ciudad')->setParameter('ciudad', $ciudad)
                ->getQuery()
                ->getResult();
        } else {
            $query = $this->createQueryBuilder('e')
                ->where('e.titulo LIKE :filter')->setParameter('filter', '%'.$filter.'%')
                ->getQuery()
                ->getResult();
        }

        return $query;

    }

    public function getCiudades() {
        return $this->createQueryBuilder('e')
            ->select('e.ciudad')
            ->groupBy('e.ciudad')
            ->getQuery()
            ->getResult();
    }

    public function getEventsFilter($titulo, $ciudad, $categoria, $order)
    {
        $qb = $this ->createQueryBuilder('e');

    if (isset($titulo)) {
        $qb->where($qb->expr()->like('e.titulo', ':titulo')) ->setParameter('titulo', '%'.$titulo.'%');
    }
    if (isset($ciudad)) {
        $qb->andWhere($qb->expr()->eq('e.ciudad', ':ciudad'))
            ->setParameter('ciudad', $ciudad);
    }

    if (isset($categoria)) {
        $qb->andWhere($qb->expr()->eq('e.categoria', ':categoria'))
            ->setParameter('categoria', $categoria);
    }

    //TODO sustituir usuario por el usuario del token
    $qb->andWhere($qb->expr()->eq('e.usuario', ':usuario')) -> setParameter('usuario', $this->getUser()->getId());

    $qb->orderBy('e.'.$order, 'ASC');
    return $qb->getQuery()->getResult(); }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('e')
            ->where('e.something = :value')->setParameter('value', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
