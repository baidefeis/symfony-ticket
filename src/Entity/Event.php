<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventRepository")
 */
class Event
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $imagen;

    /**
     * @ORM\Column(type="string")
     */
    private $titulo;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fechaInicioVenta;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fechaFinVenta;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fechaHoraEvento;

    /**
     * @ORM\Column(type="integer")
     */
    private $numeroEntradas;

    /**
     * @ORM\Column(type="decimal")
     */
    private $precioEntrada;

    /**
     * @ORM\Column(type="string")
     */
    private $descripcion;

    /**
     * @ORM\Column(type="string")
     */
    private $localizacion;

    /**
     * @ORM\Column(type="string")
     */
    private $ciudad;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $web;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="usuario", referencedColumnName="id")
     */
    private $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="categoria", referencedColumnName="id")
     */
    private $categoria;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    public function getImagen() {
        return $this->imagen;
    }

    public function setImagen($imagen) {
        $this->imagen = $imagen;
    }

    /**
     * @return mixed
     */
    public function getTitulo() {
        return $this->titulo;
    }

    /**
     * @param mixed $titulo
     */
    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    /**
     * @return mixed
     */
    public function getFechaInicioVenta() {
        return $this->fechaInicioVenta;
    }

    /**
     * @param mixed $fechaInicioVenta
     */
    public function setFechaInicioVenta($fechaInicioVenta) {
        $this->fechaInicioVenta = $fechaInicioVenta;
    }

    /**
     * @return mixed
     */
    public function getFechaFinVenta() {
        return $this->fechaFinVenta;
    }

    /**
     * @param mixed $fechaFinVenta
     */
    public function setFechaFinVenta($fechaFinVenta) {
        $this->fechaFinVenta = $fechaFinVenta;
    }

    /**
     * @return mixed
     */
    public function getFechaHoraEvento() {
        return $this->fechaHoraEvento;
    }

    /**
     * @param mixed $fechaHoraEvento
     */
    public function setFechaHoraEvento($fechaHoraEvento) {
        $this->fechaHoraEvento = $fechaHoraEvento;
    }

    /**
     * @return mixed
     */
    public function getNumeroEntradas() {
        return $this->numeroEntradas;
    }

    /**
     * @param mixed $numeroEntradas
     */
    public function setNumeroEntradas($numeroEntradas) {
        $this->numeroEntradas = $numeroEntradas;
    }

    /**
     * @return mixed
     */
    public function getPrecioEntrada() {
        return $this->precioEntrada;
    }

    /**
     * @param mixed $precioEntrada
     */
    public function setPrecioEntrada($precioEntrada) {
        $this->precioEntrada = $precioEntrada;
    }

    /**
     * @return mixed
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    /**
     * @return mixed
     */
    public function getLocalizacion() {
        return $this->localizacion;
    }

    /**
     * @param mixed $localizacion
     */
    public function setLocalizacion($localizacion) {
        $this->localizacion = $localizacion;
    }

    /**
     * @return mixed
     */
    public function getCiudad() {
        return $this->ciudad;
    }

    /**
     * @param mixed $ciudad
     */
    public function setCiudad($ciudad) {
        $this->ciudad = $ciudad;
    }

    /**
     * @return mixed
     */
    public function getWeb() {
        return $this->web;
    }

    /**
     * @param mixed $web
     */
    public function setWeb($web) {
        $this->web = $web;
    }

    /**
     * @return mixed
     */
    public function getUsuario() {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     */
    public function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    /**
     * @return mixed
     */
    public function getCategoria() {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     */
    public function setCategoria($categoria) {
        $this->categoria = $categoria;
    }



}
