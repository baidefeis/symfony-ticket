function deleteMessage() {
    let idMessage = document.getElementById('recipient-name').value;

    let request = new XMLHttpRequest();
    let url = '/messages/delete/' + idMessage;
    request.open('DELETE', url, true);
    request.send();
    request.onreadystatechange = () => {
        if (request.status === 200 && request.readyState === 4)
        {
            let respuesta = JSON.parse(request.response);
            if (respuesta.ok) {
                document.getElementById('modalButtons').innerHTML = '';
                document.getElementById('modalContent').innerHTML = 'Se ha borrado el mensaje';
                setTimeout( () => window.location.href = '/messages/me', 1000);
            } else {
                document.getElementById('modalButtons').innerHTML = '<button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>'
                document.getElementById('modalContent').innerHTML = 'No se ha podido borrar el mensaje';
            }
        }
    };
}

function deleteMessage() {
    let idEvent = document.getElementById('recipient-name').value;

    let request = new XMLHttpRequest();
    let url = '/events/delete/' + idEvent;
    request.open('DELETE', url, true);
    request.send();
    request.onreadystatechange = () => {

        if (request.readyState === 4) {
            if (request.status === 200) {
                let respuesta = JSON.parse(request.response);
                if (respuesta.ok) {
                    document.getElementById('modalButtons').innerHTML = '';
                    document.getElementById('modalContent').innerHTML = 'Se ha borrado el evento';
                } else {
                    document.getElementById('modalButtons').innerHTML = ''
                    document.getElementById('modalContent').innerHTML = 'No se ha podido borrar el evento';
                }

            } else if (request.status === 500) {
                document.getElementById('modalButtons').innerHTML = ''
                document.getElementById('modalContent').innerHTML = 'No se puede borrar el evento, es posible que se hayan vendido entradas';
            }
            setTimeout( () => window.location.href = '/events/mine', 1000);
        }
    };
}

function deleteUser() {
    let idEvent = document.getElementById('recipient-name').value;

    let request = new XMLHttpRequest();
    let url = '/user/delete/' + idEvent;
    request.open('DELETE', url, true);
    request.send();
    request.onreadystatechange = () => {

        if (request.readyState === 4) {
            if (request.status === 200) {
                let respuesta = JSON.parse(request.response);
                if (respuesta.ok) {
                    document.getElementById('modalButtons').innerHTML = '';
                    document.getElementById('modalContent').innerHTML = 'Se ha borrado el usuario';
                } else {
                    document.getElementById('modalButtons').innerHTML = ''
                    document.getElementById('modalContent').innerHTML = 'No se ha podido borrar el evento';
                }

            } else if (request.status === 500) {
                document.getElementById('modalButtons').innerHTML = ''
                document.getElementById('modalContent').innerHTML = 'No se puede borrar el usuario, es posible que tenga entradas compradas';
            }
            setTimeout( () => window.location.href = '/user/gestion', 1000);
        }
    };
}

function updateRole(idUser) {
    let role = document.getElementById('role'+idUser).value;

    let request = new XMLHttpRequest();
    let url = '/user/updateRole/' + idUser + '/' + role;
    request.open('GET', url, true);
    request.send();
    request.onreadystatechange = () => {

        if (request.readyState === 4) {
            if (request.status === 200) {
                let respuesta = JSON.parse(request.response);
                let alert = document.getElementById('alert');
                if (respuesta.ok) {
                    alert.innerHTML = 'Role actualizado';
                    alert.classList.add('alert-success');
                } else {
                    alert.innerHTML = 'Error al actualizar el role del usuario';
                    alert.classList.add('alert-danger');
                }

            } else {
                alert.innerHTML = 'Error al actualizar el role del usuario';
                alert.classList.add('alert-danger');
            }
        }
    };
}

function dataModal(data) {
    document.getElementById('recipient-name').value = data;
}